#+TITLE: Config
#+AUTHOR: Suphal Bhattarai
#+DESCRIPTION: Literate config

* TABLE OF CONTENTS :toc:
- [[#fonts][FONTS]]
- [[#theme][THEME]]
- [[#dired][DIRED]]
- [[#org-mode][ORG MODE]]
- [[#company-mode][COMPANY MODE]]
- [[#python][PYTHON]]
- [[#web][WEB]]
- [[#dictionary][DICTIONARY]]
- [[#exwm][EXWM]]
- [[#mu4e][MU4E]]
- [[#misc][MISC]]

* FONTS
There are five fonts in doom emacs for different contents and  many functionalities. They are as follows:
+ 'doom-font' -- standard monospace font that is used for most things in Emacs.
+ 'doom-variable-pitch-font' -- variable font which is useful in some Emacs plugins.
+ 'doom-big-font' -- used in doom-big-font-mode; useful for presentations.
+ 'font-lock-comment-face' -- for comments.
+ 'font-lock-keyword-face' -- for keywords with special significance, like ‘for’ and ‘if’ in C.

  #+BEGIN_SRC emacs-lisp
(setq doom-font (font-spec :family "SauceCodePro Nerd Font Mono" :size 15)
      doom-variable-pitch-font (font-spec :family "Ubuntu" :size 15)
      doom-big-font (font-spec :family "SauceCodePro Nerd Font Mono" :size 24))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
  #+END_SRC

* THEME
Setting the theme for emacs to doom-dracula.

#+BEGIN_SRC emacs-lisp
(setq doom-theme 'doom-dracula)
#+END_SRC

* DIRED
To make dired attractive I have included the treemacs icons in dired and ability to view image in dired like ranger file manager.

#+BEGIN_SRC emacs-lisp
(add-hook 'dired-mode-hook 'treemacs-icons-dired-mode)
(map!
 (:after dired
  (:map dired-mode-map
   "C-x i" #'peep-dired
   )))
#+END_SRC

* ORG MODE
These are some of my org mode configurations.

#+BEGIN_SRC emacs-lisp
(after! org
  (require 'org-bullets)  ; Nicer bullets in org-mode
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
  (setq org-directory "~/Documents/org/")
  (set-face-attribute 'org-link nil
                      :weight 'normal
                      :background nil)
  (set-face-attribute 'org-code nil
                      :foreground "#a9a1e1"
                      :background nil)
  (set-face-attribute 'org-date nil
                      :foreground "#5B6268"
                      :background nil)
  (set-face-attribute 'org-level-1 nil
                      :foreground "steelblue2"
                      :background nil
                      :height 1.2
                      :weight 'normal)
  (set-face-attribute 'org-level-2 nil
                      :foreground "slategray2"
                      :background nil
                      :height 1.0
                      :weight 'normal)
  (set-face-attribute 'org-level-3 nil
                      :foreground "SkyBlue2"
                      :background nil
                      :height 1.0
                      :weight 'normal)
  (set-face-attribute 'org-level-4 nil
                      :foreground "DodgerBlue2"
                      :background nil
                      :height 1.0
                      :weight 'normal)
  (set-face-attribute 'org-level-5 nil
                      :weight 'normal)
  (set-face-attribute 'org-level-6 nil
                      :weight 'normal)
  (set-face-attribute 'org-document-title nil
                      :foreground "SlateGray1"
                      :background nil
                      :height 1.75
                      :weight 'bold)
  (setq org-fancy-priorities-list '("⚡" "⬆" "⬇" "☕")))
(add-hook 'org-mode-hook  #'icomplete-mode)
#+END_SRC

* COMPANY MODE
I use company-box for the aesthetics and add company-backends here.

#+BEGIN_SRC emacs-lisp
(add-hook 'after-init-hook 'global-company-mode)
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)
(setq company-dabbrev-downcase 0)
(defvar-local company-box--parent-start nil)
(require 'company-box)
(add-hook 'company-mode-hook 'company-box-mode)
(add-hook 'company-mode-hook
          (lambda ()
            (substitute-key-definition
             'company-complete-common
             'company-yasnippet-or-completion
             company-active-map)))
(add-hook 'company-mode-hook 'company-tng-mode)
#+END_SRC

* PYTHON
I have elpy for python configuration and ipython or jupyter for interpreter. I found that lsp is too slow for me so I use elpy for my code completion and incorporate ide like features in emacs for python.

#+BEGIN_SRC emacs-lisp

(setq python-shell-interpreter "ipython")

(require 'py-autopep8)
(add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
;; (add-hook 'python-mode-hook #'lsp)
(add-hook 'after-init-hook 'elpy-enable)
(add-hook 'python-mode-hook #'elpy-mode)

(defun my-python-mode-hook()
  (set (make-local-variable 'company-backends) '(elpy-company-backend))
  )
(add-hook 'python-mode-hook 'my-python-mode-hook)
(setq elpy-company-post-completion-function 'elpy-company-post-complete-parens)
(setq read-process-output-max (* 2048 2048))
(setq lsp-completion-provider :capf)
(setq lsp-idle-delay 0.0)
(setq lsp-completion-enable t)
(setq lsp-semantic-highlighting t)

;; (add-hook 'python-mode-hook 'jedi:setup)
;; (setq jedi:complete-on-dot t)
;; (defun myy/python-mode-hook ()
;;   (add-to-list 'company-backends 'company-jedi))
;; (add-hook 'python-mode-hook 'myy/python-mode-hook)
;; (setq jedi:complete-on-dot t)
;; (setq jedi:get-in-function-call-delay 0.1)
;; (setq jedi:get-in-function-call-timeout 0.1)
#+END_SRC

#+RESULTS:
: t

* WEB
I do web development sometimes so I use impatient mode for live reload and some configurations for it.
#+BEGIN_SRC emacs-lisp
(defun my-web-mode-hook ()
  (set (make-local-variable 'company-backends) '(company-css company-web-html company-yasnippet company-files))
  )
(require 'prettier-js)
(add-hook 'js-mode-hook
          (lambda ()
            (add-hook 'before-save-hook 'prettier-before-save)))
#+END_SRC
* DICTIONARY
This is configuration for the dictionary for which I use aspell and english dictionary.
If RUN-TOGETHER is true, spell check the CamelCase words.
Please note RUN-TOGETHER makes aspell less capable.  So it should be used in `prog-mode-hook' only."
#+BEGIN_SRC emacs-lisp
(defvar my-default-spell-check-language "en_US"
  "Language used by aspell and hunspell CLI.")

(with-eval-after-load 'flyspell
  ;; better performance
  (setq flyspell-issue-message-flag nil))

(defun my-detect-ispell-args (&optional run-together)
  "If RUN-TOGETHER is true, spell check the CamelCase words.
Please note RUN-TOGETHER makes aspell less capable.  So it should be used in `prog-mode-hook' only."
  (let* (args)
    (when ispell-program-name
      (cond
       ;; use aspell
       ((string-match "aspell" ispell-program-name)
        (setq args (list "--sug-mode=ultra" (format "--lang=%s" my-default-spell-check-language)))
        (when run-together
          (cond
           ((string-match-p "--.*camel-case"
                            (shell-command-to-string (concat ispell-program-name " --help")))
            (setq args (append args '("--camel-case"))))
           (t
            (setq args (append args '("--run-together" "--run-together-limit=16")))))))
       ;; use hunspell
       ((string-match "hunspell" ispell-program-name)
        (setq args nil))))
    args))
#+END_SRC
* EXWM
EXWM or emacs X11 window manager is a package for emacs that turns emacs into a window manager if it is started form a display manager or from startx.
Uncomment to activate
I do not use it much but have basic configurations for it.
#+BEGIN_SRC emacs-lisp
;; (require 'exwm)
;; (require 'exwm-config)
;; (exwm-config-default)
;; (require 'exwm-randr)
;; (setq exwm-randr-workspace-output-plist '(0 "LVDS-1"))
;; (add-hook 'exwm-randr-screen-change-hook
;;           (lambda ()
;;             (start-process-shell-command
;;              "xrandr" nil "xrandr --output LVDS-1 --mode 1366x768 --pos 0x0 --rotate normal")))
;; (exwm-randr-enable)
;; (require 'exwm-systemtray)
;; (exwm-systemtray-enable)
#+END_SRC
* MU4E
mu4e is an email client for emacs that makes viewing and replying to email possible from emacs. I use imap for mail storage for my emails offline.
The aur package mu should be downloaded for mu4e to work.
#+BEGIN_SRC emacs-lisp
(require 'mu4e)
(setq mail-user-agent 'mu4e-user-agent)
(setq mu4e-drafts-folder "/Gmail/[Gmail].Drafts")
(setq mu4e-sent-folder   "/Gmail/[Gmail].Sent Mail")
(setq mu4e-trash-folder  "/Gmail/[Gmail].Trash")

(setq mu4e-sent-messages-behavior 'delete)
(setq user-full-name "Suphal Bhattarai"
      user-mail-address "suphalbhattarai4@gmail.com")
(require 'smtpmail)
(setq message-send-mail-function 'smtpmail-send-it
      starttls-use-gnutls t
      smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
      smtpmail-auth-credentials
      '(("smtp.gmail.com" 587 "suphalbhattarai4@gmail.com" nil))
      smtpmail-default-smtp-server "smtp.gmail.com"
      smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587)
(setq message-kill-buffer-on-exit t)
#+END_SRC
* MISC
This is some miscellaneous configurations for doom emacs. I use rainbow delimiters for colourful brackets and some key bindings for navigation.
There are some program mode hooks and modes for completions and other stuffs.
#+BEGIN_SRC emacs-lisp
(add-hook 'prog-mode-hook '+fold/close-all)
(add-hook 'foo-mode-hook  #'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(add-hook 'foo-mode-hook  #'fixup-whitespace)
(add-hook 'prog-mode-hook #'icomplete-mode)
(add-hook 'foo-mode-hook  #'icomplete-mode)

(map! :leader
      :desc "enable treemacs"
      "N n" #'treemacs)
(map!
 (:after dired
  (:map dired-mode-map
   "C-x i" #'peep-dired
   )))
(general-define-key :keymaps 'evil-insert-state-map
                    (general-chord "kj") 'evil-normal-state
                    (general-chord "jk") 'evil-normal-state)
(setq display-line-numbers-type t)

(define-key evil-insert-state-map (kbd "ii") 'evil-normal-state)
#+END_SRC
